import pygame

class Bullet(pygame.sprite.Sprite):

    def __init__(self, screen, gun):
        """"create bullet front gun position"""
        super(Bullet, self). __init__()
        self.screen = screen
        self.rect = pygame.Rect(0, 0, 4, 12)
        self.color = 233, 150, 122
        self.speed = 4.5
        self.rect.centerx = gun.rect.centerx
        self.rect.top = gun.rect.top
        self.y = float(self.rect.y)

    def update(self):
        """"move bullre up"""
        self.y -= self.speed
        self.rect.y = self.y

    def drow_bullet(self):
        """"paint bullet at the screen"""
        pygame.draw.rect(self.screen, self.color, self.rect)