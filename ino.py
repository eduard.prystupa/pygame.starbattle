import pygame

class Ino(pygame.sprite.Sprite):
    """"class of one UFO"""

    def __init__(self, screen):
        """"initialisation, add start position"""
        super(Ino, self).__init__()
        self.screen = screen
        self.image = pygame.image.load('images/ufo.png')
        self.rect = self.image.get_rect()
        self.rect.x = self.rect.width
        self.rect.y = self.rect.height
        self.x = float(self.rect.x)
        self.y = float(self.rect.y)

    def drow(self):
        """"take ufo on a screen"""
        self.screen.blit(self.image, self.rect)

    def update(self):
        """"move ufo"""
        self.y += 0.1
        self.rect.y = self.y
