class Stats():
    """"statistic"""

    def __init__(self):
        """"initialisation statistic"""
        self.resets_stats()
        self.run_game = True
        with open('High_score.txt', 'r') as f:
            self.hight_score = int(f.readline())

    def resets_stats(self):
        """"currently statistic, dynamic"""
        self.guns_left = 2
        self.score = 0